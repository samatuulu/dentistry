# Dent dentist

Dent dentist project is where people get in touch with dentist through website.  Dent dentist is a website where users can read about the dent and see their activity blogs and other cool interesing parts.
This website covers four pages with their static files and forms somewhere.

## Getting Started

So, before you run this project in your machine you need to have Python 3 and local environment. I am sure you already have those things. You can't skip those steps.
<h6>Note: Don't forget to activate your local environment.</h6>

### Installing

A step by step series of examples how to run project in your machine.


1.Clone project. Run this command in terminal:

```
git clone https://gitlab.com/samatuulu/dentistry.git
```

2.Install project packages. Near the file `source` you can see the file `requirements.txt`. By running this command you can install project packages.

```
pip install -r requirements.txt
```
<h6>Note: When you are running this command you need to be in that directory where the file `requirements.txt`.</h6>

3.Build your database.  Go inside the file `source` by typing `cd source` and you see python file called `manage.py`.
```shell script
python3 manage.py migrate
```

4.This step is optional. You can also backup database in file `source/webapp/fixtures`.
```shell script
python3 manage.py loaddata `fixtures/dump.json`
```
<h6>Note: If you are getting directory error, I'm sure you will figure out.


## Run project

So far, now you are ready to run the project. Here is the command.
```shell script
python3 manage.py runserver
```
after, visit to <a href="http://localhost:8000">http://localhost:8000</a> and you should see project.

## Docker
If you are docker fun, you can also run this project with Docker.

1.Create database in your Docker. Command will be:
```shell script
docker-compose run web python manage.py makemigrations
```
2. Migrate Database:
```shell script
docker-compose run web python manage.py migrate
```
3. Back up database. (Optional)
```shell script
docker-compose run web python manage.py loaddata fixtures/dump.json
```
<h6>Note: If you do not want to back up database then create it, if you don't create you get an error in `views.py` file.</h6>

4. Build a container.
```shell script
docker-compose up --build
```
That's it. Check the browser on port: <a href="http://localhost:8000">http://localhost:8000</a>



## Built With
This website build with Python/Django(backend) and used ready templates of Colorlib(front end).

* [Django](https://docs.djangoproject.com/en/3.0/) - A webframework for backend programming.
* [ColorLib](https://colorlib.com/) - Front End templates
* [Bootstrap](http://booking.com/) - A Bootstrap where you can use forms, card, buttons and other interesting things.

## Authors

* **Bektursun** - *Project author* - [Gitlab](https://gitlab.com/samatuulu)

## Contributing
Any contributions are highly appreciated.  Contributions where we can learn together and develop ourself.  Feel free to add other functionalty to the project. I will be very happy to see your work.
* Fork project
* Create your Feature Branch (`git checkout -b feature/amazingfeature`)
* Commit your Changes (`git commit -m add some amazingfeature`)
* Push to the Branch (`git push origin feature/amazingfeature`)
* Open a Pull Request