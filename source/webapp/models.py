from django.db import models


class Service(models.Model):
    name = models.CharField(max_length=2000, verbose_name='Service')
    stage = models.CharField(max_length=50, verbose_name='Stage')
    price = models.DecimalField(max_digits=10, decimal_places=2, verbose_name='Price')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Service'
        verbose_name_plural = 'Services'
        ordering = ['name']


class Dentist(models.Model):
    image = models.ImageField(upload_to='dentist_images', verbose_name='Dentist image')
    name = models.CharField(max_length=40, verbose_name='Name')
    last_name = models.CharField(max_length=40, verbose_name='Last name')
    position = models.CharField(max_length=40, verbose_name='Position')
    facebook = models.CharField(max_length=60, verbose_name='Facebook link')
    twitter = models.CharField(max_length=60, verbose_name='Twitter link')
    google = models.CharField(max_length=60, verbose_name='Google Link')

    def __str__(self):
        return "{} - {}".format(self.name, self.position)

    class Meta:
        verbose_name = 'Dentist'
        verbose_name_plural = 'Dentists'


class AboutUs(models.Model):
    description = models.TextField(max_length=2000, verbose_name='Description')

    def __str__(self):
        return self.description

    class Meta:
        verbose_name = 'About Us'
        verbose_name_plural = 'About Us'


class Blog(models.Model):
    title = models.CharField(max_length=100, verbose_name='Title')
    description = models.TextField(max_length=3000, verbose_name='First Paragraph')
    second_description = models.TextField(max_length=3000, verbose_name='Second Paragraph')
    quote = models.CharField(max_length=200, null=True, blank=True, verbose_name='Quote')
    quote_author = models.CharField(max_length=60, null=True, blank=True, verbose_name='Quote author')
    quote_author_position = models.CharField(max_length=70, null=True, blank=True, verbose_name='Quote Author Position')
    third_description = models.TextField(max_length=3000, null=True, blank=True, verbose_name='Third Paragraph')
    fourth_description = models.TextField(max_length=3000, null=True, blank=True, verbose_name='Fourth Paragraph')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Created on')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='Updated at')
    category = models.ForeignKey('webapp.Category', on_delete=models.PROTECT, null=False, blank=False,
                                 verbose_name='Blog Category',
                                 related_name='blog_category')

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Blog'
        verbose_name_plural = 'Blogs'
        ordering = ('-created_at',)


class BlogPhoto(models.Model):
    photo = models.ImageField(upload_to='blog_images', verbose_name='Photo')
    blog_id = models.ForeignKey('webapp.Blog', null=False, blank=False, on_delete=models.CASCADE,
                                 verbose_name='Blog', related_name='photo_blogs')

    class Meta:
        verbose_name = 'Blog Photo'
        verbose_name_plural = 'Blog Photos'


class Category(models.Model):
    name = models.CharField(max_length=20, verbose_name='Category')

    def __str__(self):
        return  self.name

    class Meta:
        verbose_name = 'Category'
        verbose_name_plural = 'Categories'


class UserFeedBack(models.Model):
    image = models.ImageField(upload_to='user_images', verbose_name='Photo', null=True, blank=True)
    name= models.CharField(max_length=60, verbose_name='User Name')
    l_name = models.CharField(max_length=60, verbose_name='Last Name')
    feedback = models.TextField(max_length=2000, verbose_name='Feedback')
    position = models.CharField(max_length=80, null=True, blank=True, verbose_name='Position')

    def __str__(self):
        return "{} - {}".format(self.name, self.feedback)

    class Meta:
        verbose_name = 'User Feedback'
        verbose_name_plural = 'User Feedbacks'

