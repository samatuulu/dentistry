from django.urls import path

from .views import ServiceListView, DentistListView, contact_us, Services, PricingListView, appointment, BlogListView, \
    BlogCategoryListView, BlogSearchView, BlogDetailView

urlpatterns = [
    path('', ServiceListView.as_view(), name='home'),
    path('dent/about/us/', DentistListView.as_view(), name='about_us'),
    path('dent/contact/us/', contact_us, name='contact_us'),
    path('dent/services/', Services.as_view(), name='services'),
    path('dent/pricing/', PricingListView.as_view(), name='price'),
    path('dent/appointment/', appointment, name='appointment'),
    path('dent/blogs/', BlogListView.as_view(), name='blogs'),
    path('dent/blogs/category/<int:pk>/', BlogCategoryListView.as_view(), name='category'),
    path('dent/blogs/search/', BlogSearchView.as_view(), name='search'),
    path('dent/blog/detail/<int:pk>/', BlogDetailView.as_view(), name='blog_detail')
]

app_name = 'webapp'
