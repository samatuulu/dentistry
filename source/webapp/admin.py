from django.contrib import admin

from webapp.models import Service, Dentist, AboutUs, Blog, BlogPhoto, Category, UserFeedBack


class ServiceAdmin(admin.ModelAdmin):
    list_display = ['pk', 'name', 'stage', 'price']
    list_filter = ['name', 'price']
    list_display_links = ['name']
    search_fields = ['name', 'price']
    fields = ['name', 'stage', 'price']


class DentistAdmin(admin.ModelAdmin):
    list_display = ['pk', 'name', 'last_name', 'position']
    list_filter = ['name', 'position']
    list_display_links = ['name']
    search_fields = ['name', 'position']
    fields = ['image', 'name', 'last_name', 'position', 'facebook', 'twitter', 'google']


class AboutUsAdmin(admin.ModelAdmin):
    list_display = ['pk', 'description']
    fields = ['description']


class BlogAdmin(admin.ModelAdmin):
    list_display = ['pk', 'title', 'created_at', 'category']
    list_filter = ['title', 'created_at', 'updated_at']
    list_display_links = ['title']
    search_fields = ['title', 'description']
    fields = ['title', 'description', 'second_description', 'quote', 'quote_author', 'quote_author_position',
              'third_description', 'fourth_description', 'category']


class BlogPhotoAdmin(admin.ModelAdmin):
    list_display = ['pk', 'photo', 'blog_id']
    list_display_links = ['photo', 'blog_id']
    list_filter = ['blog_id']
    search_fields = ['blog_id']
    fields = ['photo', 'blog_id']


class CategoryAdmin(admin.ModelAdmin):
    list_display = ['pk', 'name']
    list_display_links = ['name']
    list_filter = ['name']
    search_fields = ['name']
    fields = ['name']


class UserFeedBackAdmin(admin.ModelAdmin):
    list_display = ['pk', 'name', 'feedback']
    list_display_links = ['name']
    list_filter = ['name', 'l_name']
    search_fields = ['name', 'l_name']
    fields = ['image', 'name', 'l_name', 'feedback', 'position']


admin.site.register(Service, ServiceAdmin)
admin.site.register(Dentist, DentistAdmin)
admin.site.register(AboutUs, AboutUsAdmin)
admin.site.register(Blog, BlogAdmin)
admin.site.register(BlogPhoto, BlogPhotoAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(UserFeedBack, UserFeedBackAdmin)
