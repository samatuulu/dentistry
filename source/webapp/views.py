from django.contrib import messages
from django.core.paginator import Paginator
from django.db.models import Q
from django.shortcuts import render
from django.views.generic import ListView, DetailView
from django.core.mail import send_mail

from webapp.models import Service, Dentist, AboutUs, Category, Blog, UserFeedBack


class ServiceListView(ListView):
    model = Service
    template_name = 'dent/index.html'
    context_object_name = 'services'
    paginate_by = 10

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['dentists'] = Dentist.objects.all()
        context['info_description'] = AboutUs.objects.get(id=1)
        context['user_feedbacks'] = UserFeedBack.objects.all()
        blogs = Blog.objects.all().order_by('-created_at')
        paginator = Paginator(blogs, 3)
        page_number = self.request.GET.get('page')
        page_obj = paginator.get_page(page_number)
        context['blogs'] = page_obj
        return context


class DentistListView(ListView):
    model = Dentist
    template_name = 'dent/about.html'
    context_object_name = 'dentists'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['aboutus'] = AboutUs.objects.get(id=1)
        context['user_feedbacks'] = UserFeedBack.objects.all()
        return context


def contact_us(request):
    if request.method == 'POST':
        message_name = request.POST.get('message-name')
        message_email = request.POST.get('message-email')
        message = request.POST.get('message')
        # send an email
        send_mail(
            message_name, # subject
            message + " " + message_email, # message and message_email in order to know who filled out form and sent. User email can't send message directory to gmail service.
            '', # From email. Here needs to be an email that you use for smtp server.
            ['',], # To email, you can send to multiple users on form.

        )
        return render(request, 'dent/contact.html', {'message_name': message_name})
    return render(request, 'dent/contact.html', {})


class Services(ListView):
    model = Blog
    template_name = 'dent/service.html'
    context_object_name = 'blogs'
    paginate_by = 3

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data()
        context['user_feedbacks'] = UserFeedBack.objects.all()
        return  context


class PricingListView(ListView):
    model = Service
    template_name = 'dent/pricing.html'
    context_object_name = 'prices'


def appointment(request):
    if request.method == 'POST':
        your_name = request.POST.get('your-name')
        your_phone = request.POST.get('your-phone')
        your_email = request.POST.get('your-email')
        your_address = request.POST.get('your-address')
        your_schedule = request.POST.get('your-schedule')
        your_date = request.POST.get('your-date')
        your_message = request.POST.get('your-message')

        # send an email

        appointment = "Name: " + your_name + "\n" + "Phone: " + your_phone + "\n" + "Email: "\
                      + your_email + "\n" + "Address: " + your_address + "\n" + "Schedule: " + your_schedule + "\n" + "Date: " \
                      + your_date + "\n" + "Message: " + your_message
        send_mail(
            'Appointment Request', #subject
            appointment, # message
            '', # from email
            [''] # To email

        )
        return render(request, 'dent/appointment.html', {
            'your_name': your_name,
            'your_phone': your_phone,
            'your_email': your_email,
            'your_address': your_address,
            'your_schedule': your_schedule,
            'your_date': your_date,
            'your_message': your_message,
        })
    return render(request, 'dent/index.html', {})


class BlogListView(ListView):
    model = Blog
    template_name = 'dent/blog.html'
    context_object_name = 'blogs'
    paginate_by = 5

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['categories'] = Category.objects.all()
        return context


class BlogCategoryListView(ListView):
    model = Blog
    template_name = 'dent/category.html'
    context_object_name = 'category'

    def get_queryset(self):
        return Blog.objects.filter(category_id=self.kwargs.get('pk'))

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['categories'] = Category.objects.all()
        return context


class BlogSearchView(ListView):
    model = Blog
    template_name = 'dent/search.html'

    def get_queryset(self):
        query = self.request.GET.get('query')
        if query == "":
            messages.add_message(self.request, messages.WARNING, 'Please fill out the search bar.')
        object_list = Blog.objects.filter(
            Q(title__icontains=query) | Q(description__contains=query)
        )
        return object_list

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['categories'] = Category.objects.all()
        return context


class BlogDetailView(DetailView):
    model = Blog
    template_name = 'dent/blog-details.html'
    context_object_name = 'blog'

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['categories'] = Category.objects.all()
        return context
